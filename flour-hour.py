import requests
from datetime import datetime
from bs4 import BeautifulSoup
import optparse
import textwrap
import sys
import smtplib
import os
from email.message import EmailMessage

BULK_MILL_URL = "https://www.shipton-mill.com/trade/bulk-shop"
BAG_MILL_URL = "https://www.shipton-mill.com/flour-direct-shop/flour/currently-available"

def its_flour_hour(html):
    # Search for this exact text in the html. If it's not there, it's probably ok to order some flour now.
    return len(html.find_all(text="Sorry, we don't have any delivery slots available at this time, please come back later.")) == 0


def main():
    description = """\
    Pulls down Shipton Mill's website 
    """
    usage = "usage: %prog email_address"
    parser = optparse.OptionParser(
        usage=usage, description=textwrap.dedent(description)
    )
    parser.add_option(
        "-b", "--bulk", dest="bulk", action="store_true",
    )
    parser.add_option(
        "-e", "--sendemail", dest="send_email", action="store_true",
    )
    options, args = parser.parse_args(sys.argv[1:])
    if not len(args) >= 1:
        print ("You must provide at least one argument")
        return 2
    email_address = args[0]
    # Get the Shipton Mill Homepage
    if options.bulk:
        url = BULK_MILL_URL
    else:
        url = BAG_MILL_URL
    
    response = requests.get(url)
    html = BeautifulSoup(response.text, "html.parser")

    if its_flour_hour(html):
        # Send an email to the provided address
        print("[%s] It is flour hour! Emailing %s" % (datetime.now(), email_address))

        # Write it to a file so that we can inspect the form at some point
        if not os.path.isfile('flour-hour.html'):
            with open('flour-hour.html', 'w') as f:
                f.write(response.text)
                msg_wrotehtml = EmailMessage()
                msg_wrotehtml.set_content("Written the html of the page with the email form to disk!")
                msg_wrotehtml['Subject'] = "It's Flour Hour!"
                msg_wrotehtml['From'] = 'noreply@heleli.net'
                msg_wrotehtml['To'] = email_address
                s = smtplib.SMTP('localhost')
                s.send_message(msg)
                s.quit()

        msg = EmailMessage()
        body = """
        Hi!

        It looks as though there might be a slot ready at %(url)s. At least, there was at
        %(when)s UTC. This is for %(buying_type)s purchases.

        Good Luck
        """ % {
            'url': url,
            'when': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            'buying_type': options.bulk and 'bulk' or 'bag'
        }
        msg.set_content(textwrap.dedent(body))
        msg['Subject'] = "It's Flour Hour!"
        msg['From'] = 'noreply@heleli.net'
        msg['To'] = email_address

        if options.send_email:
            s = smtplib.SMTP('localhost')
            s.send_message(msg)
            s.quit()
    else:
        print("[%s] Sorry, it is not flour hour." % datetime.now())

if __name__ == "__main__":
    main()