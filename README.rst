What this is
============
A really really basic scraper to pull the Shipton Mill website and see whether there may be a booking slot available, by looking for the lack of the "there are no slots available" message.

Pre-requisites
==============

* python3
* python virtualenv installed (recommend doing this rather than polluting your system-wide python with 1000s of libraries)
* An smtp mail server running on standard ports, localhost of whatever machine you are running this on. 

Installation (Windows systems using git-bash)
=============================================

    virtualenv --no-site-packages ./v
    ./v/Scripts/pip install -r requirements.txt


Installation (Unix-type systems)
================================

    virtualenv --no-site-packages ./v
    ./v/bin/pip install -r requirements.txt

Running (Windows systems using git-bash)
========================================

    ./v/Scripts/python flour-hour.py joebloggs@example.com


Running (Unix-type systems)
===========================

    ./v/bin/python flour-hour.py joebloggs@example.com


Running (as a cron job)
=======================
*/10 * * * * /path/to/where/you/cloned/this/to/v/bin/python /path/to/where/you/cloned/this/to/flour-hour.py joebloggs@example.com



